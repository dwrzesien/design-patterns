<?php

include_once('../MysqlConnect.php');

class CreateTable {

    private $tableMaster;
    private $hookup;

    public function __construct() {
        $this->tableMaster = "helpdesk";
        $this->hookup = MysqlConnect::doConnect();
        $drop = "DROP TABLE IF EXISTS $this->tableMaster";
        if ($this->hookup->query($drop) === true) {
            printf("Stara tabela %s została usunięta.<br/>", $this->tableMaster);
        }
        $sql = "CREATE TABLE $this->tableMaster (id INT NOT NULL AUTO_INCREMENT,chain VARCHAR(3), response TEXT, PRIMARY KEY (id))";
        if ($this->hookup->query($sql) === true) {
            printf("Tabela $this->tableMaster zostala utworzona.<br/>");
        }
        $this->hookup->close();
    }

}

$worker = new CreateTable();
?>