<?php
//Maintenance.php
//konkretny dekorator
class Maintenance extends Decorator
{
    public function __construct(IComponent $siteNow)
    {
        $this->site = $siteNow;
    }
    
    public function getSite()
    {
        $format="<br/>&nbsp;&nbsp; Obsluga ";
        return $this->site->getSite() . $format;
    }
    
    public function getPrice()
    {
        return 950 + $this->site->getPrice();
    }
}

