<?php
//DataBase.php
//konkretny dekorator
class DataBase extends Decorator
{
    public function __construct(IComponent $siteNow)
    {
        $this->site = $siteNow;
    }
    
    public function getSite()
    {
        $format="<br/>&nbsp;&nbsp; Baza danych MySQL ";
        return $this->site->getSite() . $format;
    }
    
    public function getPrice()
    {
        return 800 + $this->site->getPrice();
    }
}
