<?php
//Video.php
//konkretny dekorator
class Video extends IComponent
{
    public function __construct(IComponent $siteNow)
    {
        $this->site = $siteNow;
    }
    
    public function getSite()
    {
        $format="<br/>&nbsp;&nbsp; Wideo ";
        return $this->site->getSite() . $format;
    }
    
    public function getPrice()
    {
        return 350 + $this->site->getPrice();
    }
}
