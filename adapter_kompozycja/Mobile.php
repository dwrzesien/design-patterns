<?php

include_once('IMobileFormat.php');

class Mobile implements IMobileFormat
{
    private $sampleText;
    
    public function formatCSS()
    {
        echo "plik CSS dla wersji mobile</br>";
    }
    
    public function formatGraphics()
    {
        echo "obrazek</br>";
    }
    
    public function verticalLayout()
    {
        $this->sampleText= 'tekstowy opis z bazy lub pliku';
        echo 'wertykalny layout' . ' + '. $this->sampleText;
    }
}

