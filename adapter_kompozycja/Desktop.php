<?php

include_once("IFormat.php");

class Desktop implements IFormat
{
    private $sampleText;
    
    public function formatCSS()
    {
        echo "plik CSS dla wersji desktop</br>";
    }
    
    public function formatGraphics()
    {
        echo "obrazek</br>";
    }
    
    public function horizontalLayout()
    {
        $this->sampleText= 'tekstowy opis z bazy lub pliku';
        echo 'horyzontalny layout' . ' + '. $this->sampleText; 
    }

}