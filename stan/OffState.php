<?php
//OffState.php
class OffState implements IState
{
    private $context;

    public function __construct(Context $contextNow)
    {
        $this->context=$contextNow;
    }
    
    public function turnLightOn()
    {
        echo "Wlaczyc swiatla! Narescie widze!<br/>";
        $this->context->setState($this->context->getOnState());
    }
    
    public function turnLightOff()
    {
        echo "Swiatlo jest wylaczone -> nic nie rob<br/>";
    }
}