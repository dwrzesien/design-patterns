<?php
//OnState.php
class OnState implements IState
{
    private $context;

    public function __construct(Context $contextNow)
    {
        $this->context=$contextNow;
    }
    
    public function turnLightOn()
    {
        echo "Swiatlo jest wlaczone -> nic nie rob<br/>";
    }
    
    public function turnLightOff()
    {
        echo "Wylaczyc swiatla!<br/>";
        $this->context->setState($this->context->getOffState());
    }
}
