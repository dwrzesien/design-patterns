<?php

include_once '../MysqlConnect.php';

class CreateTable {

    private $tableMaster;
    private $hookup;

    public function __construct() {
        $this->tableMaster = "cms";
        $this->hookup = MysqlConnect::doConnect();
        $drop = "DROP TABLE IF EXISTS $this->tableMaster";
        if ($this->hookup->query($drop) === true) {
            printf("Stara tabela %s zostala usunieta.<br/>", $this->tableMaster);
        }
        $sql = $this->createQuery();
        if ($this->hookup->query($sql) === true) {
            printf("Tabela $this->tableMaster zostala pomyslnie utworzona.<br/>");
        }
        $this->hookup->close();
    }
    
    private function createQuery(){
        
        $query = "CREATE TABLE $this->tableMaster 
            (
                id SERIAL,
                dpHeader NVARCHAR(50),
                textBlock TEXT,
                imageURL NVARCHAR(60),
                PRIMARY KEY (id)
            )";
        
        return $query;
    }

}

$worker = new CreateTable();
