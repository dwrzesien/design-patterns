<?php

include_once 'ConnectorInterface.php';

class MysqlConnect implements ConnectorInterface {

    private static $server = ConnectorInterface::HOST;
    private static $currentDB = ConnectorInterface::DBNAME;
    private static $user = ConnectorInterface::UNAME;
    private static $pass = ConnectorInterface::PW;
    private static $hookup;

    public static function doConnect() {
        self::$hookup = mysqli_connect(self::$server, self::$user, self::$pass, self::$currentDB);
        if (self::$hookup) {
        //komentarze na potrzeby debugowania
        } elseif (mysqli_connect_error(self::$hookup)) {
            echo('Oto twój błąd: ' . mysqli_connect_error());
        }
        return self::$hookup;
    }

}
