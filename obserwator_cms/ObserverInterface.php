<?php

interface ObserverInterface {

    function update(SubjectAbstract $subject);
}
