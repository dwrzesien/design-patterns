<?php

class Client {

    public function insert() {
        $context = new Context(new InsertData());
        $context->algorithm();
    }

    public function find() {
        $context = new Context(new SearchData());
        $context->algorithm();
    }

    public function showAll() {
        $context = new Context(new DisplayData());
        $context->algorithm();
    }

    public function update() {
        $context = new Context(new UpdateData());
        $context->algorithm();
    }

    public function delete() {
        $context = new Context(new DeleteData());
        $context->algorithm();
    }

}
