<?php

//ConcreteClass.php
include_once('AbstractClass.php');
class ConcreteClass extends AbstractClass
{
    protected function addPix($pix)
    {
        $this->pix=$pix;
        $this->pix = "pix/" . $this->pix;
        $formatter = "<em>Obrazek:</em>" . $this->pix . "<br/>";
        echo $formatter;
    }
    
    protected function addCaption($cap)
    {
        $this->cap=$cap;
        echo "<em>Podpis:</em>" . $this->cap . "<br/>";
    }
}
