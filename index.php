<?php
    include 'DirectoryListener.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="site-container">
            <p>List of Design Patterns</p>
            <ul>
                <?php
//                    ini_set("display_errors","1");
//                    ERROR_REPORTING(E_ALL);
                    
                    $toRemove = array('.', '..', '.git', '.gitignore', 
                        'DirectoryListener.php', 'index.php');
                    $DirectoryListener = new DirectoryListener(__DIR__);
                    $ListOfDirectoies = $DirectoryListener->listTheDirectory();
                    $FilteredListOfDirectories = $DirectoryListener->filter($ListOfDirectoies, $toRemove);
                ?>
                
                <?php foreach($FilteredListOfDirectories as $dir){ ?>
                        <li><a href='<?= $dir ?>'><?= $dir ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </body>
</html>
