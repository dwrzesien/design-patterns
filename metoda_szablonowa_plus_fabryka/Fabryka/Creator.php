<?php
//Creator.php
abstract class Creator
{
    protected abstract function factoryMethod();
    
    public function doFactory()
    {
        $element = $this->factoryMethod();
        return $element;
    }
}
