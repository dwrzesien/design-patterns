<?php
//Description.php
//konkretna klasa wzorca Metoda Szablonowa
//wywołuje Metodę Fabrykującą
class Description extends AbstractDescription
{
    protected function addPix()
    {
        $this->pix=new GraphicFactory();
        echo $this->pix->doFactory();
    }
    
    protected function addCaption()
    {
        $this->cap=new TextFactory();
        echo $this->cap->doFactory();
    }
}
