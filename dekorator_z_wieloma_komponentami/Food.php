<?php
//Food.php
//konkretny dekorator
class Food extends Decorator
{
    private $chowNow;
    private $snacks = array("piz"=>"Pizza",
                            "burg"=>"Hamburgery",
                            "nach"=>"Nachosy",
                            "veg"=>"Warzywa");
    
    public function __construct(IComponent $dateNow)
    {
        $this->date = $dateNow;
        $this->getDate();
    }
    
    public function setFeature($yum)
    {
        $this->chowNow=$this->snacks[$yum];
    }
    
    public function getFeature()
    {
        $output=$this->date->getFeature();
        $fmat="<br/>&nbsp;&nbsp;";
        $output .="$fmat Ulubione jedzenie: ";
        $output .= $this->chowNow;
        
        return $output;
    }
}
