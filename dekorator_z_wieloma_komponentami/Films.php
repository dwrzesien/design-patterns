<?php
//Food.php
//konkretny dekorator
class Films extends Decorator
{
    private $chowNow;
    private $snacks = array("sf"=>"Science-Fiction",
                            "th"=>"Thiller",
                            "ko"=>"Komedia",
                            "hr"=>"Horror");
    
    public function __construct(IComponent $dateNow)
    {
        $this->date = $dateNow;
        $this->getDate();
    }
    
    public function setFeature($yum)
    {
        $this->chowNow=$this->snacks[$yum];
    }
    
    public function getFeature()
    {
        $output=$this->date->getFeature();
        $fmat="<br/>&nbsp;&nbsp;";
        $output .="$fmat Ulubione gatunki filmów: ";
        $output .= $this->chowNow . "<br/>";
        
        return $output;
    }
}
