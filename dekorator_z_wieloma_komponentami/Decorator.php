<?php
ini_set("display_errors","2");
ERROR_REPORTING(E_ALL);

//Decorator.php
//uczestnik dekorator
abstract class Decorator extends IComponent
{
    public function getDate()
    {
        if($this->date != null){
           $this->date->getDate();
        }
    }
    
    public function setAge($ageNow)
    {
        $this->ageGroup=$ageNow;
    }
    
    public function getAge()
    {
        return $this->ageGroup;
    }
}

