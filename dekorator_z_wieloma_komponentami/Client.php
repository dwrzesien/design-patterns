<?php
function __autoload($class_name)
{
    include $class_name . '.php';
}

//Client.php
class Client
{
    /**
     * Grupy wiekowe
     */
    const GRUPA_18_29  = 'Grupa wiekowa 1';
    const GRUPA_30_39  = 'Grupa wiekowa 2';
    const GRUPA_40_49  = 'Grupa wiekowa 3';
    const GRUPA_50plus = 'Grupa wiekowa 4';
    
    //$hotDate to instancja komponentu
    private $hotDate;
    
    public function __construct()
    {
        $this->hotDate=new Female();
        $this->hotDate->setAge(self::GRUPA_50plus);
        echo $this->hotDate->getAge();
        $this->hotDate=$this->wrapComponent($this->hotDate);
        echo $this->hotDate->getFeature();
    }
    
    private function wrapComponent(IComponent $component)
    {
        $component=new ProgramLang($component);
        $component->setFeature("php");
        $component=new Hardware($component);
        $component->setFeature("lin");
        $component=new Food($component);
        $component->setFeature("veg");
        return $component;
    }
}
