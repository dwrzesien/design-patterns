<?php

class DirectoryListener {
   
    public $directory;
    
    public function __construct($directory) {
        $this->setDirectory($directory);
    }
    
    public function setDirectory($directory){
        $this->directory = $directory;
    }
    
    public function getDirectory(){
        return $this->directory;
    }
    
    public function listTheDirectory(){
        
        return scandir($this->getDirectory());
        
    }
    
    public function filter(array $listOfDirectory, array $toRemove){
        
        $filteredList = array();
        
        foreach($listOfDirectory as $dir){
            if(!in_array($dir, $toRemove)){
                $filteredList[] = $dir;
            }
        }
        
        return $filteredList;
    }
}
